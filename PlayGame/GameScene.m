//
//  GameScene.m
//  PlayGame
//
//  Created by Cragnarsson on 2016-04-07.
//  Copyright (c) 2016 Cecilia Ragnarsson. All rights reserved.
//

#import "GameScene.h"

@implementation GameScene

static const uint32_t fallingObjectCategory = 0x1 << 0;
static const uint32_t borderObjectCategory = 0x1 << 1;
static const uint32_t pointObjectCategory = 0x1 << 2;
static const uint32_t playerObjectCategory = 0x1 << 3;
static const uint32_t candyObjectCategory = 0x1 << 4;

-(void)didMoveToView:(SKView *)view {
    
    //set up motion manager
    self.motionManager = [[CMMotionManager alloc] init];
    [self.motionManager startAccelerometerUpdates];
    
    //set up screen display
    [self setUpScreen];
    
    //delete fps and nodecount from screen
    self.view.showsFPS = NO;
    self.view.showsNodeCount = NO;

    self.physicsWorld.contactDelegate = self;
    self.counter = 0;
    
}

-(void) startGame {
    //create candy and blocks with time intervals
    self.timerCandy1 = [NSTimer scheduledTimerWithTimeInterval:4.0f target:self selector:@selector(makeCandy1:) userInfo:nil repeats:YES];
    self.timerCandy2 = [NSTimer scheduledTimerWithTimeInterval:6.0f target:self selector:@selector(makeCandy2:) userInfo:nil repeats:YES];
    self.timerBlocks = [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(makeFallingBlock:) userInfo:nil repeats:YES];
}

- (void) makeCandy1:(NSTimer *)timer {
    SKSpriteNode *candy = [SKSpriteNode spriteNodeWithImageNamed:@"candy1"];
    candy.xScale = 0.1;
    candy.yScale = 0.1;
    candy.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:candy.size];
    candy.physicsBody.dynamic = YES;
    candy.physicsBody.affectedByGravity = NO;
    candy.name = @"candy";
    candy.position = [self randomPointWithinContainerSize:self.scene.size forViewSize:candy.size];
    
    candy.physicsBody.categoryBitMask = candyObjectCategory;
    candy.physicsBody.contactTestBitMask = playerObjectCategory;
    [self addChild: candy];
    
    SKAction *delay = [SKAction waitForDuration:5];
    SKAction *remove = [SKAction removeFromParent];
    SKAction *actionSequence = [SKAction sequence:@[delay,remove]];
    SKAction *endlessAction = [SKAction repeatActionForever:actionSequence];
    [candy runAction:endlessAction];
}

- (void) makeCandy2:(NSTimer *)timer {
    SKSpriteNode *candy = [SKSpriteNode spriteNodeWithImageNamed:@"polka"];
    candy.xScale = 0.4;
    candy.yScale = 0.4;
    candy.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:candy.size];
    candy.physicsBody.dynamic = YES;
    candy.physicsBody.affectedByGravity = NO;
    candy.name = @"candy";
    candy.position = [self randomPointWithinContainerSize:self.scene.size forViewSize:candy.size];
    
    candy.physicsBody.categoryBitMask = candyObjectCategory;
    candy.physicsBody.contactTestBitMask = playerObjectCategory;
    [self addChild: candy];
    
    SKAction *delay = [SKAction waitForDuration:3];
    SKAction *remove = [SKAction removeFromParent];
    SKAction *actionSequence = [SKAction sequence:@[delay,remove]];
    SKAction *endlessAction = [SKAction repeatActionForever:actionSequence];
    [candy runAction:endlessAction];
}

- (void) makeFallingBlock: (NSTimer *) timer {
    SKSpriteNode *fallingObject = [SKSpriteNode spriteNodeWithColor: self.randomColor size:CGSizeMake(50, 50)];
    fallingObject.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:fallingObject.size];
    int randomX = (arc4random() % (0 + 1000));
    CGPoint pos = CGPointMake(randomX, self.frame.size.height-100);
    fallingObject.position = pos;
    
    // add physics to falling SpriteNodes
    fallingObject.physicsBody.dynamic = YES;
    self.physicsWorld.gravity = CGVectorMake(0, -10);
    fallingObject.physicsBody.categoryBitMask = fallingObjectCategory;
    fallingObject.physicsBody.collisionBitMask = fallingObjectCategory | borderObjectCategory | playerObjectCategory;
    fallingObject.physicsBody.restitution = 0.3;
    fallingObject.name = @"fallingObject";
    [self addChild:fallingObject];
}


- (CGPoint) randomPointWithinContainerSize:(CGSize)containerSize forViewSize:(CGSize)size {
    CGFloat xRange = containerSize.width - size.width;
    CGFloat yRange = containerSize.height-98 - size.height;
    
    CGFloat minX = (containerSize.width - xRange) / 2;
    CGFloat minY = (containerSize.height-98 - yRange) / 2;
    
    int randomX = (arc4random() % (int)floorf(xRange)) + minX;
    int randomY = (arc4random() % (int)floorf(yRange)) + minY;
    return CGPointMake(randomX, randomY);
}


-(void)setUpScreen {
    
    // Creates a physics body that borders the screen
    SKPhysicsBody* borderBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:CGRectMake(0, 98, self.frame.size.width, self.frame.size.height)];
    self.physicsBody = borderBody;
    
    // Changes Background
    SKSpriteNode* background = [SKSpriteNode spriteNodeWithImageNamed:@"black"];
    background.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
    background.size = CGSizeMake(self.frame.size.width, self.frame.size.height);
    background.zPosition = -100;
    [self addChild:background];
    
    //Border with physics
    SKSpriteNode *border = [SKSpriteNode spriteNodeWithColor: [UIColor whiteColor] size:CGSizeMake(300, 20)];
    border.position = CGPointMake(300, 400);
    border.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:border.size];
    border.physicsBody.collisionBitMask = borderObjectCategory | fallingObjectCategory;
    border.physicsBody.affectedByGravity = NO;
    border.physicsBody.dynamic = YES;
    border.physicsBody.restitution = -20;
    [self addChild:border];
    
    //Points
    CGPoint point1 = CGPointMake(200, 400);
    CGPoint point2 = CGPointMake(400, 400);
    
    //points for springjoint
    SKSpriteNode *springPoint1 = [SKSpriteNode spriteNodeWithColor: [UIColor blackColor] size:CGSizeMake(5, 5)];
    springPoint1.position = point1;
    springPoint1.zPosition = -99;
    springPoint1.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:springPoint1.size];
    springPoint1.physicsBody.categoryBitMask = pointObjectCategory;
    springPoint1.physicsBody.collisionBitMask = pointObjectCategory;
    springPoint1.physicsBody.affectedByGravity = NO;
    springPoint1.physicsBody.dynamic = NO;
    [self addChild:springPoint1];
    
    SKSpriteNode *springPoint2 = [SKSpriteNode spriteNodeWithColor: [UIColor blackColor] size:CGSizeMake(5, 5)];
    springPoint2.position = point2;
    springPoint2.zPosition = -99;
    springPoint2.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:springPoint1.size];
    springPoint2.physicsBody.categoryBitMask = pointObjectCategory;
    springPoint2.physicsBody.collisionBitMask = pointObjectCategory;
    springPoint2.physicsBody.affectedByGravity = NO;
    springPoint2.physicsBody.dynamic = NO;
    [self addChild:springPoint2];
    
    //springjoints
    SKPhysicsJointSpring *spring1 = [SKPhysicsJointSpring jointWithBodyA:border.physicsBody bodyB:springPoint2.physicsBody anchorA:point1 anchorB:point1];
    spring1.frequency = 5.0;
    spring1.damping = 1.0;
    [self.physicsWorld addJoint:spring1];
    
    SKPhysicsJointSpring *spring2 = [SKPhysicsJointSpring jointWithBodyA:border.physicsBody bodyB:springPoint1.physicsBody anchorA:point2 anchorB:point2];
    spring2.frequency = 5.0;
    spring2.damping = 1.0;
    [self.physicsWorld addJoint:spring2];
    
    
    //WhiteBorder
    SKSpriteNode *whiteBorder = [SKSpriteNode spriteNodeWithColor: [UIColor whiteColor] size:CGSizeMake(200, 40)];
    whiteBorder.position = CGPointMake(950, 300);
    whiteBorder.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:whiteBorder.size];
    whiteBorder.physicsBody.collisionBitMask = borderObjectCategory | fallingObjectCategory;
    whiteBorder.physicsBody.affectedByGravity = NO;
    whiteBorder.physicsBody.dynamic = NO;
    [self addChild:whiteBorder];
    
    //WhiteBorder2
    SKSpriteNode *whiteBorder2 = [SKSpriteNode spriteNodeWithImageNamed:@"border"];
    whiteBorder2.position = CGPointMake(550, 200);
    whiteBorder2.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:whiteBorder2.size];
    whiteBorder2.physicsBody.collisionBitMask = borderObjectCategory | fallingObjectCategory;
    whiteBorder2.physicsBody.affectedByGravity = NO;
    whiteBorder2.physicsBody.dynamic = NO;
    [self addChild:whiteBorder2];
    
    //StartLabel
    self.startLabel = [SKSpriteNode spriteNodeWithImageNamed:@"startLabel"];
    self.startLabel.name = @"startLabel";
    self.startLabel.position = CGPointMake(600, 550);
    self.startLabel.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.startLabel.size];
    self.startLabel.physicsBody.affectedByGravity = NO;
    [self addChild:self.startLabel];
    
    //scoreLabel
    self.scoreLabel = [SKLabelNode labelNodeWithText:@"0"];
    self.scoreLabel.fontColor = [UIColor whiteColor];
    self.scoreLabel.zPosition = -98;
    self.scoreLabel.fontSize = 50;
    self.scoreLabel.position = CGPointMake(970, 600);
    [self addChild:self.scoreLabel];
    

    //make nodes userinteraction enabled
    self.userInteractionEnabled = YES;
    
    //set up playerSprite
    self.playerSprite = [SKSpriteNode spriteNodeWithImageNamed: @"gubbe2"];
    self.playerSprite.position = CGPointMake(400, 460);
    self.playerSprite.zPosition = -99;
    self.playerSprite.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.playerSprite.size];
    self.playerSprite.physicsBody.affectedByGravity = NO;
    self.playerSprite.physicsBody.dynamic = YES;
    self.playerSprite.physicsBody.mass = 0.2;
    self.playerSprite.physicsBody.density = 0.1;
    self.playerSprite.xScale = 0.6;
    self.playerSprite.yScale = 0.6;
    self.playerSprite.name = @"playerSprite";
    
    self.playerSprite.physicsBody.categoryBitMask = playerObjectCategory;
    self.playerSprite.physicsBody.contactTestBitMask = candyObjectCategory | fallingObjectCategory;
    
    [self addChild:self.playerSprite];
}


- (void) didBeginContact:(SKPhysicsContact *)contact {
    uint32_t contactTest = (contact.bodyA.categoryBitMask | contact.bodyB.categoryBitMask);
    if (contactTest == (candyObjectCategory | playerObjectCategory)) {
        if([contact.bodyA.node.name isEqualToString:@"candy"]){
            SKNode *node = contact.bodyA.node;
            [self runAction:[SKAction playSoundFileNamed:@"collect.caf" waitForCompletion:NO]];
            [node removeFromParent];
            self.counter ++;
            if(self.counter == 10){
                [self gameWon];
            }
        }else{
            SKNode *node = contact.bodyB.node;
            [self runAction:[SKAction playSoundFileNamed:@"collect.caf" waitForCompletion:NO]];
            [node removeFromParent];
            self.counter ++;
            if(self.counter == 10){
                [self gameWon];
            }
        }
        self.scoreLabel.text = [@(self.counter) stringValue];
    }
}

- (SKSpriteNode *) createWinnerLabel {
    self.win = [SKSpriteNode spriteNodeWithImageNamed:@"WinningLabel"];
    
    self.win.name = @"winNode";
    self.win.zPosition = 100;
    self.win.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    
    return self.win;
}

-(void) gameWon {
    
    self.backgroundColor = [SKColor blackColor];
    [self removeAllChildren];
    SKSpriteNode *winnerNode = [self createWinnerLabel];
    [self addChild:winnerNode];
    
    [self.timerCandy1 invalidate];
    [self.timerCandy2 invalidate];
    [self.timerBlocks invalidate];
}

-(void)bounceBlock:(SKNode *) node {
    node.physicsBody.velocity = CGVectorMake(10, 600);
    [node.physicsBody applyForce:CGVectorMake(50, 100)];
}

-(void)bouncePlayer:(SKNode *) node {
    node.physicsBody.velocity = CGVectorMake(0, 300);
    [node.physicsBody applyForce:CGVectorMake(0, 100)];
}


-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    for (UITouch *touch in touches) {
        
        CGPoint location = [touch locationInNode:self];
        
        // Check for touches in nodes
        NSArray *nodes = [self nodesAtPoint:location];
        
        if(nodes){
            for (SKNode *node in nodes) {
                if ([node.name isEqualToString:@"fallingObject"]) {
                    //Play sound and bounce when touching node
                    [self runAction:[SKAction playSoundFileNamed:@"sound.caf" waitForCompletion:NO]];
                    [self bounceBlock:node];
                    return;
                }else if ([node.name isEqualToString:@"playerSprite"]) {
                    //Play sound and bounce when touching node
                    [self runAction:[SKAction playSoundFileNamed:@"woo-hoo.caf" waitForCompletion:NO]];
                    [self bouncePlayer:node];
                    
                    return;
                }else if ([node.name isEqualToString:@"winNode"]) {
                    [self.win removeFromParent];
                    [self didMoveToView:self.view];
                    
                    return;
                }else if ([node.name isEqualToString:@"startLabel"]) {
                    [self.startLabel removeFromParent];
                    [self startGame];
                    return;
                }else {
                    // Create Falling SpriteNodes
                    SKSpriteNode *fallingObject = [SKSpriteNode spriteNodeWithColor: self.randomColor size:CGSizeMake(50, 50)];
                    fallingObject.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:fallingObject.size];
                    CGPoint pos = CGPointMake(location.x, self.frame.size.height-100);
                    fallingObject.position = pos;
                    
                    // add physics to falling SpriteNodes
                    fallingObject.physicsBody.dynamic = YES;
                    self.physicsWorld.gravity = CGVectorMake(0, -10);
                    fallingObject.physicsBody.categoryBitMask = fallingObjectCategory;
                    fallingObject.physicsBody.collisionBitMask = fallingObjectCategory | borderObjectCategory | playerObjectCategory;
                    fallingObject.physicsBody.restitution = 0.3;
                    fallingObject.name = @"fallingObject";
                    [self addChild:fallingObject];
                }
            }
        }
    }
}

- (UIColor *)randomColor {
    CGFloat red = arc4random() % 255 / 255.0;
    CGFloat green = arc4random() % 255 / 255.0;
    CGFloat blue = arc4random() % 255 / 255.0;
    UIColor *color = [UIColor colorWithRed:red green:green blue:blue alpha:1.0];
    return color;
}

//make node move with motionmanager
-(void)processUserMotionForUpdate:(NSTimeInterval)currentTime {
    CMAccelerometerData* data = self.motionManager.accelerometerData;
    if (fabs(data.acceleration.x) > 0.2 || fabs(data.acceleration.y) > 0.2) {
        [self.playerSprite.physicsBody applyForce:CGVectorMake(40.0 * data.acceleration.y, 40 * -data.acceleration.x)];
    }
}

-(void)update:(CFTimeInterval)currentTime {
[self processUserMotionForUpdate:currentTime];
    
}


@end
