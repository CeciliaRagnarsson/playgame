//
//  GameScene.h
//  PlayGame
//

//  Copyright (c) 2016 Cecilia Ragnarsson. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import <CoreMotion/CoreMotion.h>

@interface GameScene : SKScene <UIAccelerometerDelegate, SKPhysicsContactDelegate>

@property (strong) CMMotionManager *motionManager;
@property SKSpriteNode *playerSprite;
@property SKSpriteNode *startArrow;
@property SKSpriteNode *startLabel;
@property SKLabelNode *scoreLabel;

@property (nonatomic) int counter;
@property (nonatomic) NSTimer *timerCandy1;
@property (nonatomic) NSTimer *timerCandy2;
@property (nonatomic) NSTimer *timerBlocks;

@property (nonatomic) SKSpriteNode *win;


@end
