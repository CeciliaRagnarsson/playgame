//
//  GameViewController.h
//  PlayGame
//

//  Copyright (c) 2016 Cecilia Ragnarsson. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface GameViewController : UIViewController

@end
